-- This file is for replicating the use case of a user commenting out the
-- original `engine_name` line, then adding their own.
-- engine_name = "Baldur's Gate - Enhanced Edition"
engine_name = "BGEE vanilla"
engine_mode = 0 -- 0 = BGEE, 1 = BG2EE, 2 = IWDEE
