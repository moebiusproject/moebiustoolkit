-- I made up this one, it's not copied from the game files. Which is a good
-- excuse to add this comment and make this file different from the previous 2.
engine_name = "Icewind Dale - Enhanced Edition"
engine_mode = 2 -- 0 = BGEE, 1 = BG2EE, 2 = IWDEE
