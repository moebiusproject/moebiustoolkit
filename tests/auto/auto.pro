TEMPLATE = subdirs
SUBDIRS += \
    bifffile \
    calculators \
    diceroll \
    gamebrowserresourcefilter \
    gamesearcher \
    keyfile \
    pathutils \
    resourcemanager \
    tdafile \
    tlkfile \
    xplevels \
